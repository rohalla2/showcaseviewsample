package com.jeffrohalla.testappshowcaseview.app;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;


public class MainActivity extends ActionBarActivity implements OnShowcaseEventListener, View.OnClickListener {


    private ShowcaseView mShowcaseView;
    private int mCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button testButton = (Button) findViewById(R.id.button);

        Target viewTarget = new ViewTarget(R.id.button, this);

        mShowcaseView = new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(findViewById(R.id.button)))
                .setOnClickListener(this)
                .build();

//        mShowcaseView = new ShowcaseView.Builder(this)
//                .setTarget(viewTarget)
//                        // .setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
//                        .setContentTitle("ShowcaseView")
//                        .setContentText("This is highlighting the Home button")
//                        .hideOnTouchOutside()
//                        .build();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
        new ShowcaseView.Builder(this)
                .setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
                .setContentTitle("ShowcaseView")
                .setContentText("This is highlighting the Home button")
                .hideOnTouchOutside()
                .build();
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    @Override
    public void onClick(View view) {
        switch (mCounter){
            case 0:
                mShowcaseView.setShowcase(new ActionViewTarget(this,ActionViewTarget.Type.HOME),true);
                break;
            case 1:
                mShowcaseView.setTarget(Target.NONE);
                mShowcaseView.setContentTitle("Check it out");
                mShowcaseView.setContentText("You don't always need a target to showcase");
                break;
            case 2:
                mShowcaseView.hide();

        }
        mCounter++;
        //mShowcaseView.setTarget(new ActionViewTarget(this,ActionViewTarget.Type.HOME));
    }
}
